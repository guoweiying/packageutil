package Model.Files;

import ReadFiles.ReadOnlyFile;

import java.io.IOException;
import java.io.RandomAccessFile;

/**
 * Created by guoweiying on 16/7/20.
 */

public class FileInPackage implements ReadOnlyFile {
    private RandomAccessFile packageFile;
    private String fileKeyName;
    private Long offsetInPackage;
    private Long fileLen;


    public FileInPackage(String filekeyName, RandomAccessFile packageFile, Long fileLen, Long offsetInPackage)  {
        this.fileKeyName = filekeyName;
        this.packageFile = packageFile;
        this.fileLen = fileLen;
        this.offsetInPackage = offsetInPackage;

    }

    @Override
    public synchronized int readBytes(byte[] buf, int offset, int len) throws IOException {
        int diffBytes = (int)(offset + offsetInPackage - packageFile.getFilePointer());
        if (diffBytes > 0) packageFile.skipBytes(diffBytes);
        else if (diffBytes < 0) packageFile.seek(offset + offsetInPackage);
        if (len + offset > fileLen) len = (int)(fileLen - offset);
        int dataLen = packageFile.read(buf, 0, len);
        return dataLen;
    }

    @Override
    public Long fileSize() {
        return fileLen;
    }

    @Override
    public String getFileKeyName() {
        return fileKeyName;
    }

    @Override
    public void close() {}
}
