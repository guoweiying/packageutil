package Model.Files;

import java.io.File;

/**
 * Created by guoweiying on 16/7/20.
 */
public class FileInfo {
    private String filePath;
    private String fileKeyName;
    private Long fileLen;
    private Long fileOffset;

    public FileInfo(String filePath, Long fileLen) {
        this.filePath = filePath;
        this.fileLen = fileLen;
    }

    public String getFilePath() {
        return filePath;
    }

    public Long getFileLen() {
        return fileLen;
    }

    public Long getFileOffset() {
        return fileOffset;
    }

    public String getFileKeyName() { return fileKeyName; }

    public void setFileOffset(Long fileOffset) {
        this.fileOffset = fileOffset;
    }

    public void setFileKeyName(String fileKeyName) {
        this.fileKeyName = fileKeyName;
    }
}
