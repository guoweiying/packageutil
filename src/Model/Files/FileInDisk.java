package Model.Files;

import Exceptions.FileException;
import ReadFiles.ReadOnlyFile;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * Created by guoweiying on 16/7/26.
 */
public class FileInDisk implements ReadOnlyFile {
    private File file;
    private String fileKeyName;
    private BufferedInputStream inputStream = null;

    public FileInDisk(String filePathWithName, String fileKeyName) throws FileException {
        //System.out.println(filePathWithName);
        file = new File(filePathWithName);
        if (!file.exists() || !file.isFile()) throw new FileException("the file is not invalid");
        this.fileKeyName = fileKeyName;
    }


    @Override
    public int readBytes(byte[] buf, int offset, int len) throws IOException {
        if (inputStream == null) inputStream = new BufferedInputStream(new FileInputStream(file));
        return inputStream.read(buf, offset, len);
    }

    @Override
    public Long fileSize() {
        return file.length();
    }

    @Override
    public String getFileKeyName() {
        return fileKeyName;
    }

    @Override
    public void close() throws IOException {
        if (inputStream != null) inputStream.close();
    }
}
