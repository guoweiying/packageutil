package ReadFiles;

import java.io.IOException;

/**
 * Created by guoweiying on 16/7/26.
 */
public interface ReadOnlyFile {
    public int readBytes(byte[] buf, int offset, int len) throws IOException;
    public Long fileSize();
    public String getFileKeyName();
    public void close() throws IOException;
}
