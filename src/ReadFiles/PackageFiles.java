package ReadFiles;

import Exceptions.FileException;
import Model.Files.FileInDisk;
import Model.Files.FileInPackage;
import Util.Format.Align;
import Util.Format.CheckFile;
import Util.Format.ConvertBytes;
import Util.Format.FormatFileInfo;

import java.io.*;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by guoweiying on 16/7/20.
 */

public class PackageFiles {
    private ConcurrentHashMap<String, FileInPackage> filesInPackage;
    private Set<RandomAccessFile> rf_packages;
//    private Set<FileInputStream> rf_packages;
    private String rootPath = "/Users/guoweiying/Documents/HJD666666666";

    public PackageFiles() {
        filesInPackage = new ConcurrentHashMap<String, FileInPackage>();
        rf_packages = new HashSet<RandomAccessFile>();
//        rf_packages = new HashSet<FileInputStream>();
    }

    //载入指定路径下的资源包
    public void LoadPackage(String packagePath) throws FileException {
        File packageFile = new File(packagePath);
        if (!packageFile.isFile()) throw new FileException("Package is not exists!");
        try {
            RandomAccessFile rf_packageFile = new RandomAccessFile(packagePath, "r");
//            FileInputStream rf_packageFile = new FileInputStream(packageFile);
            if (!rf_packages.add(rf_packageFile)) throw new FileException("the package is already exist!");
            //System.out.println(rf_packageFile.length());
            LoadPackageInfo(rf_packageFile);
            LoadFilesIndex(rf_packageFile);
        } catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    private void LoadPackageInfo(RandomAccessFile rf_packageFile) throws IOException, FileException {
//    private void LoadPackageInfo(FileInputStream rf_packageFile) throws IOException, FileException {
        byte[] packageInfo = new byte[Align.packageInfo_TypeLen + Align.packageInfo_VersionLen];
        rf_packageFile.read(packageInfo);
        if (! CheckFile.CheckPackageInfo(packageInfo)) throw new FileException("packageInfo is wrong");
    }

    private void LoadFilesIndex(RandomAccessFile rf_packageFile) throws IOException {
//    private void LoadFilesIndex(FileInputStream rf_packageFile) throws IOException {
        byte[] buf = new byte[Align.intLen];
        rf_packageFile.read(buf, 0, Align.intLen);
        int indexLen = ConvertBytes.Bytes2Int(buf);
        buf = new byte[indexLen];
        rf_packageFile.read(buf, 0, indexLen);
        int numOfFiles = ConvertBytes.Bytes2Int(buf, 0);
        int offset = Align.intLen;
        for (int i = 0; i < numOfFiles; ++i) {
            int fileKeyNameLen = ConvertBytes.Bytes2Int(buf, offset);
            offset += Align.intLen;
            String fileKeyName = ConvertBytes.Bytes2String(buf, offset, fileKeyNameLen);
            offset += fileKeyNameLen;
            Long fileLen = ConvertBytes.Bytes2Long(buf, offset);
            offset += Align.longLen;
            Long fileOffset = ConvertBytes.Bytes2Long(buf, offset);
            offset += Align.longLen;
            FileInPackage fileInPackage = new FileInPackage(fileKeyName, rf_packageFile, fileLen, fileOffset);

            if (filesInPackage.containsKey(fileKeyName)) {
                System.out.println("The file \"" + fileKeyName + "\" is over written");
            }
            //System.out.println(fileKeyName);
            filesInPackage.put(fileKeyName, fileInPackage);
        }
        //for (Map.Entry<String, FileInPackage> entry: filesInPackage.entrySet()) {
        //    System.out.println(entry.getKey() + " " + entry.getValue().getFileKeyName() + " " + entry.getValue().getFileLen());
        //}
    }

    public ReadOnlyFile GetFile(String fileKeyName) throws FileException {
        FileInPackage fileInPackage;
        //System.out.println(fileKeyName);
        fileInPackage = filesInPackage.get(FormatFileInfo.FormatFileName(fileKeyName));
        if (fileInPackage == null) {
            //System.out.println("文件不在包内");
            FileInDisk fileInDisk = new FileInDisk(rootPath + fileKeyName, fileKeyName);
            return fileInDisk;
        }
        else return fileInPackage;
    }

    public boolean Exists(String fileKeyName) {
        if (filesInPackage.get(fileKeyName) == null) return false;
        else return true;
    }

    public void Close() throws IOException {
        for (RandomAccessFile rf_packagefile: rf_packages) {
            rf_packagefile.close();
        }
        filesInPackage.clear();
    }

    public void setRootPath(String rootPath) {
        this.rootPath = rootPath;
    }

    public Map<String, FileInPackage> getHashMap() {
        return filesInPackage;
    }
}
