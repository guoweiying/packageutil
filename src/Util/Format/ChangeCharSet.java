package Util.Format;

import java.io.UnsupportedEncodingException;

/**
 * Created by guoweiying on 16/7/20.
 */
public class ChangeCharSet {
    public static final String UTF_8 = "UTF-8";

    public static byte[] toUTF_8(String str) throws UnsupportedEncodingException {
        return ChangeCharset(str, UTF_8);
    }

    private static byte[] ChangeCharset(String str, String newCharset)
            throws UnsupportedEncodingException {
        byte[] bs = null;
        if (str != null) {
            //用默认字符编码解码字符串。
            bs = str.getBytes(newCharset);
        }
        return bs;
    }
}
