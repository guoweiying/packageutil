package Util.Format;

/**
 * Created by guoweiying on 16/7/20.
 */
public class FormatFileInfo {
    public static String FormatFilePath(String filePath) {
        filePath = DisableCaseSensitive(FormatSlash(filePath));
        return filePath;
    }

    public static String FormatFileName(String fileName) {
        fileName = DisableCaseSensitive(FormatSlash(fileName));
        return fileName;
    }

    private static String FormatSlash(String str) {
        if (str == null) return null;
        else return str.replaceAll("\\\\", "/");
    }

    private static String DisableCaseSensitive(String str) {
        if (str == null) return null;
        else return str.toLowerCase();
    }
}
