package Util.Format;

/**
 * Created by guoweiying on 16/7/21.
 */
public class Align {
    public static final int shortLen = 2;
    public static final int intLen = 4;
    public static final int longLen = 8;
    public static final int packageInfo_TypeLen = 4;
    public static final int packageInfo_VersionLen = 2;
    public static final int alignByte = 8;

    public static Long AlignBytes(Long length, int alignByte) {
        if (length % alignByte == 0) return length;
        else return (length / alignByte + 1) * alignByte;
    }

    public static Long Align8Bytes(Long length) {
        return AlignBytes(length, alignByte);
    }
}
