package Util.Format;

import java.lang.reflect.Array;
import java.util.Arrays;

/**
 * Created by guoweiying on 16/7/20.
 */
public class CheckFile{
    private static byte[] packageType = {'_', '_', '_', 'p'};
    private static short packageVersion = 1;

    public static byte[] getPackageType() {
        return packageType;
    }

    public static short getPackageVersion() {
        return packageVersion;
    }

    //public static boolean CheckFileName(String fileName) throws FileException {
    //    String sFileName = fileName.toLowerCase();
    //    if (sFileName.equals(fileName)) return true;
    //    else return false;
    //}

    public static boolean CheckPackageInfo(byte[] _packageInfo) {
        //System.out.println(_packageType + " " + packageType);
        //System.out.println(ConvertBytes.Bytes2Short(_packageVersion) + " " + packageVersion);
        byte[] _packageType = new byte[Align.packageInfo_TypeLen];
        byte[] _packageVersion = new byte[Align.packageInfo_VersionLen];
        for (int i = 0; i < Align.packageInfo_TypeLen; ++i) _packageType[i] = packageType[i];
        if (!Arrays.equals(packageType, _packageType)) return false;
        for (int i = 0; i < Align.packageInfo_VersionLen; ++i)
            _packageVersion[i] = _packageInfo[i + Align.packageInfo_TypeLen];
        if (ConvertBytes.Bytes2Short(_packageVersion) != packageVersion) return false;
        return true;
    }
}
