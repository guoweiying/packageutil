package Util.Format;

/**
 * Created by guoweiying on 16/7/21.
 */
public class ConvertBytes {
    public static byte[] Short2Bytes(short shortNum) {
        byte[] result = new byte[Align.shortLen];
        for (int i = 0; i < Align.shortLen; ++i) {
            result[i] = (byte) (shortNum >> (8 * i) & 0xFF);
        }
        return result;
    }

    public static byte[] Int2Bytes(int intNum) {
        byte[] result = new byte[Align.intLen];
        for (int i = 0; i < Align.intLen; ++i) {
            result[i] = (byte) (intNum >> (8 * i) & 0xFF);
        }
        return result;
    }

    public static byte[] Long2Bytes(Long longNum) {
        byte[] result = new byte[Align.longLen];
        for (int i = 0; i < Align.longLen; ++i) {
            result[i] = (byte) (longNum >> (8 * i) & 0xFF);
        }
        return result;
    }

    public static int Bytes2Short(byte[] shortBuf) {
        short result = 0;
        result += (shortBuf[0] & 0xFF);
        result += ((shortBuf[1] & 0xFF) << 8);

        return result;
    }

    public static int Bytes2Int(byte[] intBuf) {
        int result = 0;
        for (int i = 0; i < Align.intLen; ++i) {
            result = result + ((intBuf[i] & 0xFF ) << (8 * i));
        }
        return result;
    }

    public static int Bytes2Int(byte[] intBuf, int offset) {
        byte[] buf = new byte[Align.intLen];
        for (int i = 0; i < Align.intLen; ++i) buf[i] = intBuf[offset + i];
        return Bytes2Int(buf);
    }

    public static Long Bytes2Long(byte[] longBuf) {
        Long result = new Long(0);
        for (int i = 0; i < Align.longLen; ++i) {
            result = result + ((longBuf[i] & 0xFF ) << (8 * i));
        }
        return result;
    }

    public static Long Bytes2Long(byte[] longBuf, int offset) {
        byte[] buf = new byte[Align.longLen];
        for (int i = 0; i < Align.longLen; ++i) buf[i] = longBuf[offset + i];
        return Bytes2Long(buf);
    }

    public static String Bytes2String(byte[] stringBuf, int offset, int len) {
        byte[] buf = new byte[len];
        for (int i = 0; i < len; ++i) buf[i] = stringBuf[offset + i];
        return new String(buf);
    }
}
