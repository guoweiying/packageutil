package Exceptions;

/**
 * Created by guoweiying on 16/7/18.
 */
public class FileException  extends Exception{
    public FileException() {
        super();
    }

    public FileException(String s) {
        super(s);
        System.out.println(s);
    }
}
