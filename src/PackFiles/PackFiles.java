package PackFiles;

import Exceptions.FileException;
import Model.Files.FileInfo;

import java.io.File;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by guoweiying on 16/7/15.
 */
public class PackFiles {
    private String packagePath = null;
    private String rootFilePath = null;
    private List<FileInfo> fileList = null;
    private List<String> filesPath = null;
    private List<String> filesKeyName = null;
    private ListFiles listFiles;
    private FilesManager filesManager;
    private WritePackage writePackage;


    private void PackFilesInCommon(String _packagePath) {
        packagePath = _packagePath;
        listFiles = new ListFiles();
        filesManager = new FilesManager();
    }

    public PackFiles(List<String> _filesPath, List<String> _filesKeyName, String _packagePath) {
        PackFilesInCommon(_packagePath);
        filesPath = _filesPath;
        filesKeyName = _filesKeyName;
    }

    public PackFiles(String _rootFilePath, String _packagePath) {
        PackFilesInCommon(_packagePath);
        rootFilePath = _rootFilePath;
//        System.out.println(_rootFilePath + "\n" + _packagePath);
    }

    public void DoPack() throws FileException, IOException {
        if (rootFilePath == null) {
            fileList = listFiles.CountFile(filesPath);
        } else {
            File rootFile;
//            System.out.println(rootFilePath);
            rootFile = new File(rootFilePath);
            //File rootFile = new File("/Users/guoweiying/Documents/HJD666666666");
            if (rootFile.exists()) {
                ListFiles listFiles = new ListFiles();
                fileList = listFiles.CountFile(rootFile);
//                System.out.println(fileList.size());
            } else throw new FileException("rootFile is not exists!");
        }
        if (filesKeyName == null) {
            filesManager.setFileKeyName(fileList, rootFilePath);
        }
        else {
            filesManager.setFileKeyName(fileList, filesKeyName);
        }
//        for (FileInfo fileInfo: fileList) {
//            System.out.println(fileInfo.getFilePath() + " " + fileInfo.getFileKeyName());
//        }
        filesManager.SortFiles(fileList);
        writePackage = new WritePackage();
        writePackage.WriteIntoPackage(fileList, packagePath);
    }

    public Map<String, String> getFilesKeyName() throws FileException {
        Map<String, String> filesKeyName = new LinkedHashMap<String, String>();
        if (writePackage == null) throw new FileException("you are not do pack");
        for (FileInfo fileInfo: fileList) {
            filesKeyName.put(fileInfo.getFilePath(), fileInfo.getFileKeyName());
        }
        return filesKeyName;
    }

    public List<FileInfo> getFileList() {
        return fileList;
    }
}
