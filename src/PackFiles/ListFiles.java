package PackFiles;

import Exceptions.FileException;
import Model.Files.FileInfo;
import Util.Format.FormatFileInfo;

import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * Created by guoweiying on 16/7/18.
 */

public class ListFiles {
    private Queue<String> dirList;
    private List<FileInfo> fileList;

    public ListFiles() {
        fileList = new ArrayList<FileInfo>();
        dirList = new LinkedList<String>();
    }

    public List<FileInfo> CountFile(File rootFile) throws IOException, FileException {
        dirList.add(rootFile.getCanonicalPath());
        while (!dirList.isEmpty()) {
            String dirPath = dirList.poll();
            File file = new File(dirPath);
            String[] subFilesPath = file.list();
            for (String subFilePath : subFilesPath) {
//                System.out.println(subFilePath);
                File subFile = new File(dirPath + "/" +subFilePath);
                if (subFile.isDirectory()) {
                    dirList.offer(subFile.getCanonicalPath());
                } else {
                    subFilePath = subFile.getAbsolutePath();
//                    System.out.println(subFilePath);
                    subFilePath = FormatFileInfo.FormatFilePath(subFilePath);
                    FileInfo fileInfo = new FileInfo(subFilePath, subFile.length());
//                    System.out.println(subFilePath);
                    fileList.add(fileInfo);
                }
            }
        }
        return fileList;
    }

    public List<FileInfo> CountFile(List<String> filesPath) throws FileException {
        for (String filePath: filesPath) {
            File file = new File(filePath);
            if (!file.exists()) throw new FileException(filePath + " is not exists!");
            else {
                FileInfo fileInfo = new FileInfo(filePath, file.length());
                fileList.add(fileInfo);
            }
        }
        return fileList;
    }
}
