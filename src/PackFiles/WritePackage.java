package PackFiles;

import Exceptions.FileException;
import Model.Files.FileInfo;
import Util.Format.Align;
import Util.Format.ChangeCharSet;
import Util.Format.CheckFile;
import Util.Format.ConvertBytes;

import java.io.*;
import java.util.List;


/**
 * Created by guoweiying on 16/7/18.
 */
public class WritePackage {

    private RandomAccessFile CreateFile(String filePathWithName) throws FileException, IOException {
        //System.out.println(System.getProperty("user.dir") + "/Compress.dat");
        File packageFile = new File(filePathWithName);
        RandomAccessFile rf_packageFile;
        if (!packageFile.exists()) {
            packageFile.createNewFile();
        } else {
            throw new FileException("资源包已存在");
        }
        rf_packageFile = new RandomAccessFile(packageFile, "rw");
        return rf_packageFile;
    }

    public void WriteIntoPackage(List<FileInfo> fileList, String rootFilePathWithName) throws IOException, FileException {
        RandomAccessFile rf_packageFile;
        rf_packageFile = CreateFile(rootFilePathWithName);
        Long packageInfoLen = WritePackageInfo(rf_packageFile);
//        System.out.println(rf_packageFile.getFilePointer());
        CalculateFileOffset(fileList, packageInfoLen);
        WriteIndex(rf_packageFile, packageInfoLen, fileList);
//        System.out.println(rf_packageFile.getFilePointer() + " " + fileList.get(0).getFileOffset());
        WriteFiles(rf_packageFile, fileList);
        try {
            rf_packageFile.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private Long WritePackageInfo(RandomAccessFile rf_packageFile) throws IOException {
        rf_packageFile.write(CheckFile.getPackageType());
        rf_packageFile.write(ConvertBytes.Short2Bytes(CheckFile.getPackageVersion()));
        return rf_packageFile.getFilePointer();
    }

    private void CalculateFileOffset(List<FileInfo> fileList, Long packageInfoLen) throws UnsupportedEncodingException {
        Long offset = packageInfoLen;
        offset += (Align.intLen + Align.intLen);
        for (FileInfo fileInfo: fileList) {
//            System.out.print(fileInfo.getFileKeyName() + " " + offset + " ");
            offset = offset + Align.intLen + ChangeCharSet.toUTF_8(fileInfo.getFileKeyName()).length + Align.longLen + Align.longLen;
            //（文件名长度 ＋ 文件keyName ＋ 文件长度 ＋ 文件偏移量）所占空间
//            System.out.println(offset);
        }

        for (FileInfo fileInfo: fileList) {
            offset = Align.Align8Bytes(offset);
            fileInfo.setFileOffset(offset);
            Long formatFileLen = fileInfo.getFileLen();
            offset += formatFileLen;
        }
    }

    private void WriteIndex(RandomAccessFile rf_packageFile, Long packageInfoLen, List<FileInfo> fileList) throws IOException {
        int numOfFiles = fileList.size();
        int indexLength = fileList.get(0).getFileOffset().intValue() - packageInfoLen.intValue();
        rf_packageFile.write(ConvertBytes.Int2Bytes(indexLength));
        rf_packageFile.write(ConvertBytes.Int2Bytes(numOfFiles));
//        System.out.println(numOfFiles + " " + indexLength);

        for (FileInfo fileInfo: fileList) {
//            System.out.println(rf_packageFile.getFilePointer());
            String fileKeyName = fileInfo.getFileKeyName();
            Long fileLen = fileInfo.getFileLen();
//            System.out.print(rf_packageFile.getFilePointer() + " ");
            rf_packageFile.write(ConvertBytes.Int2Bytes(fileKeyName.length()));
            rf_packageFile.write(ChangeCharSet.toUTF_8(fileKeyName));
            rf_packageFile.write(ConvertBytes.Long2Bytes(fileLen));
            rf_packageFile.write(ConvertBytes.Long2Bytes(fileInfo.getFileOffset()));

//            System.out.println(rf_packageFile.getFilePointer());
        }
    }

    private void WriteFiles(RandomAccessFile rf_packageFile, List<FileInfo> fileList) throws IOException, FileException {
        BufferedInputStream inbs = null;
        byte[] buf = new byte[2048];
        int len;
        for (FileInfo fileInfo: fileList) {
            try {
                inbs = new BufferedInputStream(new FileInputStream(new File(fileInfo.getFilePath())));
                int offsetDistance = (int)(fileInfo.getFileOffset() - rf_packageFile.getFilePointer());
                if (offsetDistance < 0 || offsetDistance / 8 != 0) {
                    System.out.println(fileInfo.getFileKeyName() + " " + fileInfo.getFileOffset() + " " + rf_packageFile.getFilePointer());
                    throw new FileException("file offset is wrong");
                }
                rf_packageFile.seek(fileInfo.getFileOffset());
                while ((len = inbs.read(buf)) > 0) {
                    rf_packageFile.write(buf, 0, len);
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (inbs != null) {
                    try {
                        inbs.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
}
