package PackFiles;

import Exceptions.FileException;
import Model.Files.FileInfo;
import Util.Format.FormatFileInfo;

import java.util.*;

/**
 * Created by guoweiying on 16/7/22.
 */
public class FilesManager {

    public void setFileKeyName(List<FileInfo> fileList, List<String> filesKeyName) throws FileException {
        if (fileList.size() != filesKeyName.size()) throw new FileException("The the Num of filesKeyName doesn't match filesNum");
        for (int i = 0; i < fileList.size(); ++i) {
            String keyName = FormatFileInfo.FormatFileName(filesKeyName.get(i));
            fileList.get(i).setFileKeyName(keyName);
        }
    }

    public void setFileKeyName(List<FileInfo> fileList, String rootFilePath) {
        for (FileInfo fileInfo: fileList) {
//            System.out.println(fileInfo.getFilePath());
            String keyName = fileInfo.getFilePath().substring(rootFilePath.length());
            keyName = FormatFileInfo.FormatFileName(keyName);
            fileInfo.setFileKeyName(keyName);
        }
    }

    public static List<FileInfo> SortFiles(List<FileInfo> fileList) {
        Collections.sort(fileList, new StringComparator());
        return fileList;
    }

    static class StringComparator implements Comparator {
        public int compare(Object object1, Object object2) {
            FileInfo f1 = (FileInfo) object1;
            FileInfo f2 = (FileInfo) object2;

            String fpath1 = f1.getFilePath();
            String fpath2 = f2.getFilePath();
            return fpath1.compareTo(fpath2);
        }
    }
}
