package Test;

import Exceptions.FileException;
import Model.Files.FileInfo;
import PackFiles.FilesManager;
import PackFiles.ListFiles;
import ReadFiles.PackageFiles;
import ReadFiles.ReadOnlyFile;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by gwy on 7/26/16.
 */
public class SortedAccessThread {
    public void Access(String sourcePath) throws IOException, FileException {

        List<FileInfo> fileInfoList;

        fileInfoList = new ListFiles().CountFile(new File(sourcePath));
        fileInfoList = FilesManager.SortFiles(fileInfoList);
        PackageFiles packageFiles = new PackageFiles();
        packageFiles.LoadPackage("/Users/guoweiying/IdeaProjects/packageutil/compress.dat");
        List<String> fileList = new ArrayList<String>();
        for (FileInfo fileInfo : fileInfoList) {
            String filePath = fileInfo.getFilePath();
            fileList.add(filePath.substring(sourcePath.length()));
        }
        double sumSpeed = 0;
        long timestart, timend;
        int filesNum = fileInfoList.size();
        double size = 0, time;
        System.out.println(filesNum);

        timestart = System.nanoTime();
        for (String fileKeyName : fileList) {
            ReadOnlyFile file = packageFiles.GetFile(fileKeyName);
            getSortedAccessThread thread = new getSortedAccessThread(file);
            new Thread(thread).start();
            size += (double) file.fileSize() / 1024 / 1024;
        }
        timend = System.nanoTime();
        time = (double) (timend - timestart) / 1000000000;
        sumSpeed = size / time;
//        packageFiles.Close();
        System.out.println(sumSpeed);
    }
}

class getSortedAccessThread implements Runnable {
    ReadOnlyFile file;

    getSortedAccessThread(ReadOnlyFile file) {
        this.file = file;
    }

    @Override
    public void run() {
        byte[] buf = new byte[2048];
        try {
            int len = file.fileSize().intValue();
            while (len > 0) {
                int readLen = file.readBytes(buf, 0, 2048);
                len -= readLen;
                //if (len != 2048) System.out.println(file.getFileKeyName() + " " + len);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
