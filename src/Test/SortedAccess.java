package Test;

import Exceptions.FileException;
import Model.Files.FileInfo;
import ReadFiles.*;
import PackFiles.*;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by guoweiying on 16/7/26.
 */
public class SortedAccess {

    public void Access(String sourcePath) throws FileException, IOException {
        List<FileInfo> fileInfoList;
        fileInfoList = new ListFiles().CountFile(new File(sourcePath));
        fileInfoList = FilesManager.SortFiles(fileInfoList);
        PackageFiles packageFiles = new PackageFiles();
        packageFiles.LoadPackage("/Users/guoweiying/IdeaProjects/packageutil/compress.dat");
        List<String> fileList = new ArrayList<String>();
        for (FileInfo fileInfo : fileInfoList) {
            String filePath = fileInfo.getFilePath();
            fileList.add(filePath.substring(sourcePath.length()));
        }
        double size = 0;
        double sumSpeed = 0;
        byte[] buf = new byte[2048];
        long timestart, timend;
        long len;
        int filesNum = fileInfoList.size();
        double speed = 0, time = 0;
        System.out.println(filesNum);


        for (String fileKeyName : fileList) {
            ReadOnlyFile file = packageFiles.GetFile(fileKeyName);
            timestart = System.nanoTime();
//            System.out.println(file.getFileKeyName());
            len = file.fileSize();
            while (len > 0) {
                int readLen = file.readBytes(buf, 0, 2048);
                len -= readLen;
                //if (len != 2048) System.out.println(file.getFileKeyName() + " " + len);
            }
            timend = System.nanoTime();
            size = (double) file.fileSize() / 1024 / 1024;
            time = (double) (timend - timestart) / 1000000000;

            if (time > 0) speed = size / time;
            else {
                speed = 0;
                filesNum --;
            }
            //if (size == 0) {
            //    System.out.println(fileKeyName + " " + size + " " + time + " " + speed);
            //}
            sumSpeed = sumSpeed + speed;
        }
        packageFiles.Close();
        System.out.println(sumSpeed / filesNum);
    }
}
