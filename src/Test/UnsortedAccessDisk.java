package Test;

import Exceptions.FileException;
import Model.Files.FileInPackage;
import Model.Files.FileInfo;
import PackFiles.ListFiles;
import ReadFiles.PackageFiles;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by gwy on 7/26/16.
 */
public class UnsortedAccessDisk {
    String rootPath = "/Users/guoweiying/Documents/HJD666666666";
    public void Access() throws FileException, IOException {
        List<FileInfo> fileInfoList;
        fileInfoList = new ListFiles().CountFile(new File(rootPath));
        //PackageFiles packageFiles = new PackageFiles();
        //packageFiles.LoadPackage("/Users/guoweiying/IdeaProjects/packageutil/compress.dat");
        Map<String, String> files = new HashMap<String, String>();
        for (FileInfo fileInfo: fileInfoList) files.put(fileInfo.getFilePath(), fileInfo.getFilePath());
        List<String> fileList = new ArrayList<String>();
        for (Map.Entry<String, String> entry: files.entrySet()) {
            fileList.add(entry.getKey());
        }

        double size = 0;
        double sumSpeed = 0;
        byte[] buf = new byte[2048];
        long timestart, timend;
        long len;
        int filesNum = fileInfoList.size();
        double speed = 0, time = 0;
        System.out.println(filesNum);

        for (String filePath: fileList) {
            timestart = System.nanoTime();
            File diskFile = new File(filePath);
            len = diskFile.length();
            FileInputStream fileinputStream = new FileInputStream(diskFile);
            while (len > 0) {
                int readLen = fileinputStream.read(buf, 0, 2048);
                len -= readLen;
            }
            fileinputStream.close();
            timend = System.nanoTime();
            size = (double) diskFile.length() / 1024 / 1024;
            time = (double) (timend - timestart) / 1000000000;

            if (time > 0) speed = size / time;
            else {
                speed = 0;
                filesNum --;
            }
            sumSpeed = sumSpeed + speed;
        }
        System.out.println(sumSpeed / filesNum);
    }
}
