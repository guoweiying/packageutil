package Test;

import Exceptions.FileException;
import PackFiles.*;

import java.io.IOException;

/**
 * Created by gwy on 7/26/16.
 */
public class TestPack {
    public void Pack(String sourcePath, String packagePath) {
        try {
            PackFiles packFiles;
            packFiles = new PackFiles(sourcePath, packagePath);
            packFiles.DoPack();

        } catch (FileException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
