package Test;

import Exceptions.FileException;
import Model.Files.FileInPackage;
import Model.Files.FileInfo;
import PackFiles.FilesManager;
import PackFiles.ListFiles;
import ReadFiles.PackageFiles;
import ReadFiles.ReadOnlyFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.concurrent.CountDownLatch;

/**
 * Created by gwy on 7/27/16.
 */
public class UnsortedAccessDiskThread {
    String rootPath = "/Users/guoweiying/Documents/HJD666666666";
    CountDownLatch latch;
    double sumSpeed = 0;
    int filesNum;

    public void Access() throws IOException, FileException, InterruptedException {

        PackageFiles packageFiles = new PackageFiles();
        packageFiles.LoadPackage("/Users/guoweiying/IdeaProjects/packageutil/compress.dat");
        Map<String, FileInPackage> files = packageFiles.getHashMap();
        List<String> fileList = new ArrayList<String>();
        for (Map.Entry<String, FileInPackage> entry: files.entrySet()) {
            fileList.add(rootPath + entry.getKey());
        }

        filesNum = fileList.size();
        System.out.println(filesNum);

        latch = new CountDownLatch(fileList.size());

        for (String filePath : fileList) {
            File file = new File(filePath);
            getUnsortedAccessDiskThread thread = new getUnsortedAccessDiskThread(file);
            new Thread(thread).start();
        }
        latch.await();
        //        packageFiles.Close();
        System.out.println(sumSpeed / filesNum);
    }

    class getUnsortedAccessDiskThread implements Runnable {
        File file;

        getUnsortedAccessDiskThread(File file) {
            this.file = file;
        }

        @Override
        public void run() {
            double timestart = System.nanoTime();
            double timend;
            double time;
            byte[] buf = new byte[2048];
            try {
                int len = (int) file.length();
                FileInputStream fileinputStream = new FileInputStream(file);
                while (len > 0) {
                    int readLen = fileinputStream.read(buf, 0, 2048);
                    len -= readLen;
                }
                fileinputStream.close();
                latch.countDown();
                timend = System.nanoTime();
                time = (timend - timestart) / 1000000000;
                double filesize = (double) (file.length()) / 1024 / 1024;
                double speed = (filesize) / time;
                synchronized (this) {
                    sumSpeed += speed;
                    //System.out.println(filesize + " " + time + " " + speed);
                }
                //System.out.println(latch.getCount());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}


