package Test;

import Exceptions.FileException;
import Model.Files.FileInPackage;
import Model.Files.FileInfo;
import PackFiles.FilesManager;
import PackFiles.ListFiles;
import ReadFiles.PackageFiles;
import ReadFiles.ReadOnlyFile;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by gwy on 7/26/16.
 */
public class UnsortedAccessThread {

    public void Access() throws IOException, FileException {

        PackageFiles packageFiles = new PackageFiles();
        packageFiles.LoadPackage("/Users/guoweiying/IdeaProjects/packageutil/compress.dat");
        Map<String, FileInPackage> files = packageFiles.getHashMap();
        List<String> fileList = new ArrayList<String>();
        for (Map.Entry<String, FileInPackage> entry: files.entrySet()) {
            fileList.add(entry.getKey());
        }

        double sumSpeed = 0;
        long timestart, timend;
        int filesNum = fileList.size();
        double size = 0, time;
        System.out.println(filesNum);

        timestart = System.nanoTime();
        for (String fileKeyName : fileList) {
            ReadOnlyFile file = packageFiles.GetFile(fileKeyName);
            getUnsortedAccessThread thread = new getUnsortedAccessThread(file);
            new Thread(thread).start();
            size += (double) file.fileSize() / 1024 / 1024;
        }
        timend = System.nanoTime();
        time = (double) (timend - timestart) / 1000000000;
        sumSpeed = size / time;
//        packageFiles.Close();
        System.out.println(sumSpeed);
    }
}

class getUnsortedAccessThread implements Runnable {
    ReadOnlyFile file;

    getUnsortedAccessThread(ReadOnlyFile file) {
        this.file = file;
    }

    @Override
    public void run() {
        byte[] buf = new byte[2048];
        try {
            int len = file.fileSize().intValue();
            while (len > 0) {
                int readLen = file.readBytes(buf, 0, 2048);
                len -= readLen;
                //if (len != 2048) System.out.println(file.getFileKeyName() + " " + len);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
