package Test;

import Exceptions.FileException;
import Model.Files.FileInPackage;
import ReadFiles.PackageFiles;
import ReadFiles.ReadOnlyFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by guoweiying on 16/7/26.
 */
public class UnsortedAccess {
    String rootPath = "/Users/guoweiying/Documents/HJD666666666";
    public void Access() throws FileException, IOException {
        PackageFiles packageFiles = new PackageFiles();
        packageFiles.LoadPackage("/Users/guoweiying/IdeaProjects/packageutil/compress.dat");
        Map<String, FileInPackage> files = packageFiles.getHashMap();
        List<String> fileList = new ArrayList<String>();
        for (Map.Entry<String, FileInPackage> entry: files.entrySet()) {
            fileList.add(entry.getKey());
        }

        double size = 0;
        double sumSpeed = 0;
        double speed = 0, time = 0;
        byte[] buf = new byte[2048];
        long timestart, timend;
        long len;
        int filesNum = files.size();

        System.out.println(filesNum);

        for (String fileKeyName: fileList) {
            ReadOnlyFile file = packageFiles.GetFile(fileKeyName);
            timestart = System.nanoTime();
            len = file.fileSize();
            while (len > 0) {
                int readLen = file.readBytes(buf, 0, 2048);
                len -= readLen;
                //if (len != 2048) System.out.println(file.getFileKeyName() + " " + len);
            }
            timend = System.nanoTime();
            size = (double) file.fileSize() / 1024 / 1024;
            time = (double) (timend - timestart) / 1000000000;

            if (time > 0) speed = size / time;
            else {
                speed = 0;
                filesNum --;
            }
            //if (size == 0) {
            //    System.out.println(fileKeyName + " " + size + " " + time + " " + speed);
            //}
            sumSpeed = sumSpeed + speed;
            //System.out.println(sumSpeed);
        }
        packageFiles.Close();
        System.out.println(sumSpeed / filesNum);
    }
}
