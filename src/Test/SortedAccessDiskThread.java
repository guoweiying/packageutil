package Test;

import Exceptions.FileException;
import Model.Files.FileInfo;
import PackFiles.FilesManager;
import PackFiles.ListFiles;
import ReadFiles.PackageFiles;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;

/**
 * Created by gwy on 7/26/16.
 */
public class SortedAccessDiskThread {
    CountDownLatch latch;
    double sumSpeed = 0;
    int filesNum;

    public void Access(String sourcePath) throws FileException, IOException, InterruptedException {
        List<FileInfo> fileInfoList;
        fileInfoList = new ListFiles().CountFile(new File(sourcePath));
        fileInfoList = FilesManager.SortFiles(fileInfoList);
        PackageFiles packageFiles = new PackageFiles();
        packageFiles.LoadPackage("/Users/guoweiying/IdeaProjects/packageutil/compress.dat");
        List<String> fileList = new ArrayList<String>();
        for (FileInfo fileInfo : fileInfoList) {
            fileList.add(fileInfo.getFilePath());
        }

        filesNum = fileInfoList.size();
        System.out.println(filesNum);

        latch = new CountDownLatch(fileList.size());

        for (String filePath: fileList) {
            File file = new File(filePath);
            getSortedAccessDiskThread thread =new getSortedAccessDiskThread(file);
            new Thread(thread).start();
        }
        latch.await();
        System.out.println(sumSpeed / filesNum);
        //System.out.println(size);
    }

    class getSortedAccessDiskThread implements Runnable{
        File file;

        getSortedAccessDiskThread(File file) {
            this.file = file;
        }

        @Override
        public void run() {
            double timestart = System.nanoTime();
            double timend;
            double time;
            byte[] buf = new byte[2048];
            try {
                int len = (int) file.length();
                FileInputStream fileinputStream = new FileInputStream(file);
                while (len > 0) {
                    int readLen = fileinputStream.read(buf, 0, 2048);
                    len -= readLen;
                }
                fileinputStream.close();
                latch.countDown();
                timend = System.nanoTime();
                time = (timend - timestart) / 1000000000;
                double filesize = (double) (file.length()) / 1024 / 1024;
                double speed = (filesize) / time;
                synchronized (this) {
                    sumSpeed += speed;
                    //System.out.println(filesize + " " + time + " " + speed);
                }
                //System.out.println(latch.getCount());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }


}
